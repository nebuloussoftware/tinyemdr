/*
	Vanishing Dot - Experimental meditation tool
    Copyright (C) 2022 Michael Cistera

    If you have suggestions, feedback, or otherwise wish to contact me,
    please email emdrtool@mail.com

    This file is part of Vanishing Dot.

    Vanishing Dot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vanishing Dot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
"use strict";

var dot
var start_time, end_time //timer start and end times, in ms
var active = false //true if timer is running or dot is shrinking
var lightMode = false //light background enabled?

var maxMinutes = 60 //maximum timer value
var defaultMinutes = 3 //default timer value
var secondsUntilShrink = defaultMinutes*60 //time in seconds until shrink begins

const easeTime = 10 //time in seconds over which to smoothly accelerate shrinking to full value
const shrinkTime = 30 //time in seconds for total shrink duration (assuming no easing)
const scale = 1/20 //proportion of initial screen width the dot should take

const app = new PIXI.Application({
    width: 400,
    height: 320,
    autoResize: true,
    backgroundColor: 0x000000,
    antialias: true,
});

document.body.appendChild(app.view);

app.view.onclick = function(e) {
    //go fullscreen if user clicks on the canvas
    if(!isFullscreen()){

    	if(!active){
    		secondsUntilShrink = $("#timer").val() * 60
    		start_time = Date.now()
    		end_time = start_time + secondsUntilShrink*1000
    	}
    	active = true

        this.requestFullscreen()
        $("canvas").addClass("nocursor")
    }
    else{
        //exit fullscreen if user clicks during fullscreen
        document.exitFullscreen();
        $("canvas").removeClass("nocursor")
    }
}

	
//resize window and recenter dot
function resizeScreen() {
    app.renderer.resize(Math.floor(window.innerWidth), Math.floor(getCanvasHeight()));
    if(dot){
    	dot.x = app.renderer.width / 2;
    	dot.y = app.renderer.height / 2;
    	refreshDot(lightMode)
	}
}

window.addEventListener('resize', resizeScreen);


function resetTimer(){
	end_time = null
	updateTimeRemaining()
}

function updateTimeRemaining(){
	let remainingSeconds
	if(!end_time){
		remainingSeconds = $("#timer").val() * 60
	}
	else {
		remainingSeconds = (1+ end_time - Date.now())/1000
	}
	if(remainingSeconds < 0){
		$("#time-remaining").html("Shrinking...")
		return
	}
	let minutes = Math.floor(remainingSeconds/60)
	let seconds = Math.floor(remainingSeconds%60)
	$("#time-remaining").html(`${minutes}:${padZero(seconds, 2)}`)
}

function refreshDot(lightMode=false){
	let x = dot.x, y = dot.y, scalex = dot.scale.x, scaley = dot.scale.y
	app.stage.removeChild(dot)
	dot = new PIXI.Graphics()
	dot.x = x
	dot.y = y
	dot.scale.x = scalex
	dot.scale.y = scaley
	if(lightMode){
		//reduced contrast to alleviate visual ghosting
		dot.beginFill(0x5f5f5f)
	}
	else {
		dot.beginFill(0x9f9f9f)
	}
	dot.drawCircle(0, 0, app.renderer.width * scale/2);
	dot.endFill();
	app.stage.addChild(dot)
}

app.loader.load(()=>{

    $("#light-mode-checkbox").checkboxradio().change(function(){
    	if(this.checked){
			refreshDot(false)
			app.renderer.backgroundColor = 0xffffff
    	}
    	else {
			refreshDot(true)
			app.renderer.backgroundColor = 0x000000
    	}
    })

    $("#timer").spinner({
    	min: 0,
    	max: maxMinutes,
    	step: 0.5,
    	stop: function( event, ui ) {
    		let timer = event.target
    		if(isNaN(timer.value)){
    			if(!isNaN(parseInt(timer.value))){
    				timer.value = parseInt(timer.value)
    			}
    			else {
    				timer.value = defaultMinutes
    			}
    		}
	        if (timer.value > maxMinutes ) {
	        	timer.value = maxMinutes
	        }
	        else if (timer.value < 0 ) {
	          timer.value = 0
	        }
	        updateTimeRemaining()
	        return true
      	}
    })

    $("#stop").button().click(()=>{
		active = false
		dot.scale.x = 1
		dot.scale.y = 1
		resetTimer()
    	if(!app.stage.children.includes(dot)){ //If dot disappeared, add it back
    		app.stage.addChild(dot)
    	}
    	refreshDot(lightMode)
    })

    resizeScreen() //initialize renderer and dot to available window size

    dot = new PIXI.Graphics();
	dot.beginFill(0x9f9f9f);
	dot.drawCircle(0, 0, app.renderer.width * scale/2);
	dot.endFill();
	app.stage.addChild(dot)
	dot.x = app.renderer.width / 2
	dot.y = app.renderer.height / 2

	//initialize timer
	start_time = Date.now()
	resetTimer()

	let last_t = 0
    app.ticker.add(() => {

    	if(!active){
    		return
    	}

    	let t = (Date.now() - start_time) / 1000 //time in seconds since started
    	let delta = t - last_t //delta time this update

    	updateTimeRemaining()

    	if(t > secondsUntilShrink){ //time is up, begin shrinking the dot

    		let shrinkAmount = (delta / shrinkTime)

			//slowly, linearly ease in the shrinking
			let easeFactor = 1- (easeTime - (t - secondsUntilShrink))/easeTime
			if(easeFactor <= 1){
				shrinkAmount = shrinkAmount * easeFactor
			}

			//shrink dot
			dot.scale.x -= shrinkAmount
			dot.scale.y -= shrinkAmount

	    	//remove dot once it passes below 0 scale
	    	if(dot.scale.x <= 0 || dot.scale.y <= 0){
	    		app.stage.removeChild(dot)
	    	}
    	}

    	last_t = t
    })
})