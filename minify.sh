#!/bin/bash

uglifyjs main.js -c -m -o main.min.js
uglifyjs dot.js -c -m -o dot.min.js
uglifyjs common.js -c -m -o common.min.js
uglifycss style.css --output style.min.css

cat minlicense main.min.js > main.min.js.temp && mv main.min.js.temp main.min.js
cat minlicense_dot dot.min.js > dot.min.js.temp && mv dot.min.js.temp dot.min.js
cat minlicense common.min.js > common.min.js.temp && mv common.min.js.temp common.min.js
cat minlicense style.min.css > style.min.css.temp && mv style.min.css.temp style.min.css
