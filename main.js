/*
    TinyEMDR - Eye Movement Desensitization and Reprocessing tool
    Copyright (C) 2022-2023 Michael Cistera

    If you have suggestions, feedback, or otherwise wish to contact me,
    please email emdrtool@mail.com

    This file is part of TinyEMDR.

    TinyEMDR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TinyEMDR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

"use strict";

const app = new PIXI.Application({
    width: 400,
    height: 320,
//    autoResize: true
});

document.body.appendChild(app.view);

app.view.onclick = function(e) {
    //go fullscreen if user clicks on the canvas
    if(!isFullscreen()){
        this.requestFullscreen()
        $("canvas").addClass("nocursor")
    }
    //exit fullscreen if user clicks during fullscreen
    else{
        document.exitFullscreen();
        $("canvas").removeClass("nocursor")
    }
}


const resources_dir = './resources/'
var targetParams = {}
var target = null
var latest_time = 0

window.addEventListener('resize', resizeScreen);

//resize window and recenter target
function resizeScreen() {
    let width = $("body").prop("clientWidth");
    app.renderer.resize(Math.floor(width), Math.max(200, Math.floor(getCanvasHeight())));
    targetParams.original_x = app.renderer.width / 2;
    targetParams.original_y = app.renderer.height / 2;
}


//set target speed to "speed" oscillations per minute
function setTargetSpeed(speed){
    $("#speed_amount").val(speed);
    $('#speed_slider').slider('value', speed);

    let slider = $("#speed_slider")
    slider.val(0)

    let old_speed_factor = targetParams.speedFactor

    if(!targetParams.phaseOffset){
        targetParams.phaseOffset = 0
    }

    targetParams.speedFactor = speed/60

    //Below: Phase correction when adjusting speed to remove jittering when speed changes

    //sin(2*pi*t1+old_p) = sin(2*pi*(t2+p))
    //t1 + old_p % 1 = t2+p % 1
    //p % 1 = (t1 + old_p) - t2 % 1

    //calculate the just previous t value, accounting for the old phase offset
    let t1 = latest_time * old_speed_factor
    //t2 is the value that t would take using the new speed if we do not correct for phase
    let t2 = latest_time * targetParams.speedFactor

    targetParams.phaseOffset = (t1 + targetParams.phaseOffset - t2) % 1
}

//set target size to "size", from 0 to 1
function setTargetSize(size){
    $("#size_amount").val(size);
    $('#size_slider').slider('value', size);
    target.scale.x = size*2
    target.scale.y = size*2
}

//Blends motion waveform from square wave (ratio 0) to blended wave (ratio 0..1) to sine wave (ratio 1)
function setTargetSmoothness(ratio){
    $("#smoothness_amount").val(ratio);
    $('#smoothness_slider').slider('value', ratio);
    targetParams.smoothness = ratio
}

//Sets width of X motion, range 0 (no motion) to 1 (full screen width)
function setTargetWidth(width){
    $("#width_amount").val(width);
    $('#width_slider').slider('value', width);
    targetParams.widthScale = width

    if(width == 0 ){
        togglePhaseSlider(false)
    } 
    else if (targetParams.heightScale != 0){
        togglePhaseSlider(true)
    }
}

//Sets height of Y motion, range -1(full screen height, reversed) to 0 (no motion) to 1 (full screen height)
function setTargetHeight(height){
    $("#height_amount").val(height);
    $('#height_slider').slider('value', height);
    targetParams.heightScale = height

    if(height == 0){
       togglePhaseSlider(false)
    } 
    else if (targetParams.widthScale != 0){
        togglePhaseSlider(true)
    }

}

//Toggles the phase slider on and off. Visual effect and tooltip only.
function togglePhaseSlider(enable){
    if($("#phase_slider").hasClass("ui-slider")){
        let phase = $("#phase_slider")
        if(enable){
            phase.removeClass("fake-disable")
            phase.tooltip("disable")
        } 
        else {
            phase.addClass("fake-disable")
            phase.tooltip()
            phase.tooltip("enable")
       }
   }
}

//Sets phase difference between X and Y, range 0 (out of phase) to 1 (in phase)
function setTargetPhase(phase){
    $("#phase_amount").val(phase);
    $('#phase_slider').slider('value', phase);
    targetParams.phase = phase
}

//Create the UI elements including about link, preset buttons, sliders, color pickers, and target dropdown
function createUI(){
    //About dialog
    $("#about").hide()
    
    $("#about_link").click(() => {
        $("#about").dialog({width: 500})
    })

    //===Left UI menu===

    //Preset buttons
    $("#preset_standard").button({
    }).click(()=>{
        setTargetSpeed(40)
        setTargetSize(0.25)
        setTargetSmoothness(1)
        setTargetWidth(0.8)
        setTargetHeight(0)
        setTargetPhase(0)
    })

    $("#preset_pendulum").button({
    }).click(()=>{
        setTargetSpeed(30)
        setTargetSize(0.2)
        setTargetSmoothness(1)
        setTargetWidth(0.8)
        setTargetHeight(0.25)
        setTargetPhase(0)
    })

    $("#preset_binary").button({
    }).click(()=>{
        setTargetSpeed(40)
        setTargetSize(0.25)
        setTargetSmoothness(0)
        setTargetWidth(0.8)
        setTargetHeight(0)
        setTargetPhase(0)
    })

    $("#preset_binary_four").button({
    }).click(()=>{
        setTargetSpeed(30)
        setTargetSize(0.2)
        setTargetSmoothness(0)
        setTargetWidth(0.8)
        setTargetHeight(0.6)
        setTargetPhase(1)
    })

    $("#preset_whirl").button({
    }).click(()=>{
        setTargetSpeed(30)
        setTargetSize(0.2)
        setTargetSmoothness(1)
        setTargetWidth(0.8)
        setTargetHeight(0.5)
        setTargetPhase(1)
    })


    //Sliders

    //Speed slider
    $( "#speed_slider" ).slider({
      min: 0,
      max: 80,
      value: 40,
      step: 0.25,
      animate: true,
      slide: function( event, ui ) {
        $("#speed_amount").val( ui.value );
        setTargetSpeed(ui.value)
      }
    });
    //initialize target speed value
    let speed_value = $("#speed_slider").slider("option", "value")
    setTargetSpeed(speed_value)
    $('label[for="speed_amount"]').tooltip()


    //Size slider
    $( "#size_slider" ).slider({
      min: 1/512,
      max: 1,
      value: 0.25,
      step: 1/1024,
      animate: true,
      slide: function( event, ui ) {
        $("#size_amount").val( ui.value );
        setTargetSize(ui.value)
      }
    });

    //initialize target scale value
    let size_value = $("#size_slider").slider("option", "value")
    setTargetSize(size_value)
    $('label[for="size_amount"]').tooltip()


    //Sine/Square "smoothness" slider
    $( "#smoothness_slider" ).slider({
      min: 0,
      max: 1,
      value: 1,
      step: 1/1024,
      animate: true,
      slide: function( event, ui ) {
        $("#smoothness_amount").val( ui.value );
        setTargetSmoothness(ui.value)
      }
    });

    //initialize target smoothness value
    let smoothness_value = $("#smoothness_slider").slider("option", "value")
    setTargetSmoothness(smoothness_value)
    $('label[for="smoothness_amount"]').tooltip()


    //Width slider
    $( "#width_slider" ).slider({
      min: 0,
      max: 1,
      value: 0.8,
      step: 1/1024,
      animate: true,
      slide: function( event, ui ) {
        $("#width_amount").val( ui.value );
        setTargetWidth(ui.value)
      }
    });

    //initialize width value
    let width_value = $("#width_slider").slider("option", "value")
    setTargetWidth(width_value)
    $('label[for="width_amount"]').tooltip()


    //Height slider
    $( "#height_slider" ).slider({
      min: -1,
      max: 1,
      value: 0,
      step: 1/32,
      animate: true,
      slide: function( event, ui ) {
        $("#height_amount").val( ui.value );
        setTargetHeight(ui.value)
      }
    });

    //initialize height value
    let height_value = $("#height_slider").slider("option", "value")
    setTargetHeight(height_value)
    $('label[for="height_amount"]').tooltip()


    //Phase slider
    $( "#phase_slider" ).slider({
      min: 0,
      max: 1,
      value: 0,
      step: 1/1024,
      animate: true,
      slide: function( event, ui ) {
        $("#phase_amount").val( ui.value );
        setTargetPhase(ui.value)
      }
    });
    $( "#phase_slider" ).prop("title", "Phase only has an effect when both height and width are nonzero").tooltip({})

    //initialize phase value
    let phase_value = $("#phase_slider").slider("option", "value")
    setTargetPhase(phase_value)
    $('label[for="phase_amount"]').tooltip()

    setTargetHeight(0) //necessary to initialize the "disabled" state of the phase slider


    //===Right UI menu ===
    $( "#target_image_menu" ).selectmenu({
      change: function( event, data ) {
        setTargetImage(data.item.value)
      }
     });

    let bgColorPicker =  $("#bg_color_picker").spectrum({
        type: "component",
        change: function(tiny_color) {
            setBgColor(tiny_color.toHex())
        }
    })

    bgColorPicker.spectrum("set", "#000000")

    //Allow clicking on the color preview box to open the color picker
    bgColorPicker.parent().click( (event)=>{
        bgColorPicker.spectrum("toggle")
        return false
    })

    let targetColorPicker =  $("#target_color_picker").spectrum({
        type: "component",
        change: function(tiny_color) {
            setTargetColor(tiny_color.toHex())
        }
    })


    targetColorPicker.spectrum("set", "#ffffff")

    //Allow clicking on the color preview box to open the color picker
    targetColorPicker.parent().click( (event)=>{
        targetColorPicker.spectrum("toggle")
        return false
    })
}

//make the play/mute audio UI button
function makeAudioButton(){
    function updateButton(){
        let p = ""
        let $icon = $("<span></span>").addClass("ui-icon")
        if(enableAudio){
            p = " Mute"
            $icon.addClass("ui-icon-volume-on")
        }
        else {
            p = " Play"
            $icon.addClass("ui-icon-volume-off")
        }
        $("#audio").html(p).prepend($icon)
    }
    $("#audio").button().click(
        ()=>{
            enableAudio = !enableAudio
            updateButton()
        })
    updateButton()
}

//if tick is true, play "tick" audio, else play "tock" audio
function playTickAudio(tick){
    if(!enableAudio){
        return
    }
    if(runningInBackround){
        return
    }
    if(tick){
        $("#tock_player").stop()
        $("#tick_player")[0].currentTime = 0
        $("#tick_player")[0].play().then().catch(error => {}); //catch error, if autoplay is disabled we start muted
    }else {
        $("#tick_player").stop()
        $("#tock_player")[0].currentTime = 0
        $("#tock_player")[0].play().then().catch(error => {});
    }
}
//gets the currently selected target image name
function getSelectedTargetImageName(){
    return $("#target_image_menu")[0].value
}

//gets the currently selected target image
function getSelectedTargetImage(){
    let name = getSelectedTargetImageName()
    return targetImages[name]
}

//Set the target's image to the image referred to by newImgName
function setTargetImage(newImgName){
        if(target){
            app.stage.removeChild(target)
        }
        targetParams.name = newImgName
        target = targetImages[newImgName]
        app.stage.addChild(target)
        target.x = app.renderer.width / 2;
        target.y = app.renderer.height / 2;
        targetParams.original_x = target.x;
        targetParams.original_y = target.y;
        target.anchor.x = 0.5;
        target.anchor.y = 0.5;

        //if the slider isn't initialized yet, don't set the scale, the code
        //that initializes the slider will initialize the scale value itself
        if($("#size_slider").hasClass("ui-slider")){
            let size_value = $("#size_slider").slider("option", "value")
            setTargetSize(size_value)
        }

        //if the target color picker isn't initialized yet, don't set the color, the code
        //that initializes the color picker will initialize the target color itself
        if($("#target_color_picker").hasClass("spectrum")){
            let tint = $("#target_color_picker").spectrum("get").toHex()
            setTargetColor(tint)
        }
}

//set background color to "color" (hex string)
function setBgColor(color){
    app.renderer.backgroundColor = parseInt(color, 16);
}

//set target color to "color" (hex string)
function setTargetColor(color){
    target.tint = parseInt(color, 16);
}

var enableAudio = true
var runningInBackround = false

var targetImages = {}

//Load all target images
app.loader.add('Fuzz', resources_dir +'fuzzball_512.png')
.add('Ball', resources_dir + 'circle_512.png')
.add('Target', resources_dir + 'target_512.png')
.add('Target Red', resources_dir + 'target_red_512.png')
.add('Spyro 1', resources_dir + 'spyro_1_512.png')
.add('Spyro 2', resources_dir + 'spyro_2_512.png')
.add('Light', resources_dir + 'light_768.png')
.load((loader, resources) => {    
    //After resources loaded:

    makeAudioButton()

    $(window).blur(function(){ runningInBackround = true })

    $(window).focus(function(){ runningInBackround = false })

    //initialize targetImages as list of target Sprites
    for(let img in resources){
        targetImages[img] = new PIXI.Sprite(resources[img].texture)
    }
    setTargetImage(getSelectedTargetImageName())

    //center the target
    target.x = app.renderer.width / 2;
    target.y = app.renderer.height / 2;
    
    //set oscillation center
    targetParams.original_x = target.x;
    targetParams.original_y = target.y;

    //make target position refer to the center of the target
    target.anchor.x = 0.5;
    target.anchor.y = 0.5;

    createUI()
    
    resizeScreen();
    resizeScreen(); //Twice because second time around will factor in scroll bar, if any

    app.stage.addChild(target);
    
    const start_time = Date.now()
    let last_t = 0
    
    //main loop
    app.ticker.add(() => {

        latest_time = (Date.now() - start_time) / 1000

        //calculate time value t based on the time, a speed factor, and phase corrections
        let t = targetParams.speedFactor * latest_time + (targetParams.phaseOffset? targetParams.phaseOffset : 0)//one cycle per second * speedFactor
        

        //Both x and y coordinates independently follow a sin wave
        //Two y oscillations for one x oscillation
        //This forms a lissajous curve of ratio 2:1

        //The sine waves are rectified (absolute value) in order to "squarify" them by taking a root (targetParams.smoothness from 0 to 1)
        //The square component then un-rectifies the wave by flipping every second segment

        //Calculate x components
        let sine_component_x = Math.pow(Math.abs(Math.sin(t*2*Math.PI)),targetParams.smoothness)
        let square_component_x = Math.pow((-1),Math.floor(2*t))

        //target has reached the end of the range of motion, play tick or tock sound
        if(last_t % 1 < 0.25 && t % 1 >= 0.25){
            playTickAudio(false)
        }
        else if(last_t % 1 < 0.75 && t % 1 >= 0.75){
            playTickAudio(true)
        }

        last_t = t

        //Calculate y components
        let phase = 1 - targetParams.phase
        let sine_component_y = Math.pow(Math.abs( Math.sin((t+phase/8)*4*Math.PI)), targetParams.smoothness)
        let square_component_y = Math.pow((-1), Math.floor(4*t+phase/2))

        //Set target position
        target.x = targetParams.original_x + (window.innerWidth * targetParams.widthScale/2) * sine_component_x * square_component_x
        target.y = targetParams.original_y + (getCanvasHeight() *targetParams.heightScale/2)* sine_component_y * square_component_y
    });
});



