/*
    TinyEMDR - Eye Movement Desensitization and Reprocessing tool
    Copyright (C) 2022-2023 Michael Cistera

    If you have suggestions, feedback, or otherwise wish to contact me,
    please email emdrtool@mail.com

    This file is part of TinyEMDR.

    TinyEMDR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TinyEMDR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

function isFullscreen(){
    return !!document.fullscreenElement
}

//gets the height available for the canvas
function getCanvasHeight(){
    return isFullscreen() ? window.innerHeight : window.innerHeight - $("#menubar").height()
}

function padZero(num, places){
	return String(num).padStart(places, '0')
}